// g++ -g tests.cpp -o tests
// valgrind --leak-check=full -v ./tests

#include <algorithm>
#include "mini_test.h"
#include "PlanificadorRR.h"

using namespace std;

string remove_spaces(const string& s) {
  string out(s);
  out.erase(remove(out.begin(), out.end(), ' '), out.end());
  out.erase(remove(out.begin(), out.end(), '\n'), out.end());
  return out;
}

template<typename T>
string to_s(const T& m) {
 	ostringstream os;
	os << m;
	return os.str();
}

/**
 * Crea un planificador sin porocesos.
 */
void planificadorVacio()
{
  PlanificadorRR<int> planificador;
  ASSERT_EQ(planificador.hayProcesos(), false);
  ASSERT_EQ(planificador.hayProcesosActivos(), false);
  ASSERT_EQ(planificador.cantidadDeProcesos(), 0);
  ASSERT_EQ(to_s(planificador), "[]");
}

/**
* Crea un planificador y le agregar un proceso
*/
void planificadorConProcesos() {
    PlanificadorRR<int> planificador;
    for( int i = 0; i < 1000 ; i++) {
      planificador.agregarProceso(i);
    }

    ASSERT_EQ(planificador.hayProcesosActivos(), true);
    ASSERT_EQ(planificador.cantidadDeProcesos(), 1000);
    ASSERT_EQ(planificador.cantidadDeProcesosActivos(), 1000);
    
    for(int i = 0; i<1000;i++) {
      ASSERT_EQ(planificador.procesoEjecutado(), i);  
      planificador.ejecutarSiguienteProceso();
    }
    
}


void planificadorAgregaProcesoEjecutaYAgrega() {
    PlanificadorRR<int> planificador;
    for( int i = 0; i < 500 ; i++) {
      planificador.agregarProceso(i);
    }

    ASSERT_EQ(planificador.hayProcesosActivos(), true);
    ASSERT_EQ(planificador.cantidadDeProcesos(), 500);
    ASSERT_EQ(planificador.cantidadDeProcesosActivos(), 500);
    

    for(int i = 0; i<=250;i++) {
      ASSERT_EQ(planificador.procesoEjecutado(), i);  
      planificador.ejecutarSiguienteProceso();
    }

    
    for( int i = 0; i < 500 ; i++) {
      planificador.agregarProceso(i);
    }

    ASSERT_EQ(planificador.hayProcesosActivos(), true);
    ASSERT_EQ(planificador.cantidadDeProcesos(), 1000);
    ASSERT_EQ(planificador.cantidadDeProcesosActivos(), 1000);

    ASSERT_EQ(planificador.procesoEjecutado(), 251);  

}


void crearUnoBorrarlo() {
  PlanificadorRR<int> plan;
  plan.agregarProceso(1);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 1);  
  plan.eliminarProceso(1);
  ASSERT_EQ(plan.hayProcesosActivos(), false);
  ASSERT_EQ(plan.cantidadDeProcesos(), 0);
}

void crearVariosYBorrarlo() {
  PlanificadorRR<int> plan;
  plan.agregarProceso(1);
  plan.agregarProceso(2);
  plan.agregarProceso(3);
  plan.agregarProceso(4);
  plan.agregarProceso(5);
  
 // ASSERT_EQ(plan.hayProcesosActivos(), true);
  //ASSERT_EQ(plan.cantidadDeProcesos(), 5);
    
  plan.eliminarProceso(1);
  plan.eliminarProceso(2);
  plan.eliminarProceso(3);
  plan.eliminarProceso(4);
  plan.eliminarProceso(5);
  
  ASSERT_EQ(plan.hayProcesosActivos(), false);
  ASSERT_EQ(plan.cantidadDeProcesos(), 0);
}


void crearEjecutarYBorrar() {
  PlanificadorRR<int> plan;
  plan.agregarProceso(1);

  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 1);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);  
  
  plan.ejecutarSiguienteProceso();
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 1);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);

  plan.agregarProceso(2);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);  

  plan.ejecutarSiguienteProceso();
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);  
  ASSERT_EQ(plan.procesoEjecutado(), 2);
  plan.eliminarProceso(2);
 
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 1);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);

  plan.agregarProceso(2);
  
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);

  plan.ejecutarSiguienteProceso();
 
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);  
  ASSERT_EQ(plan.procesoEjecutado(), 2);

  plan.eliminarProceso(1);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 1);  
  ASSERT_EQ(plan.procesoEjecutado(), 2);
}


void crearPausearReanudar() {
  PlanificadorRR<int> plan;
  plan.agregarProceso(0);
  plan.agregarProceso(1);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);  
  ASSERT_EQ(plan.procesoEjecutado(), 0);  

  plan.pausarProceso(0);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);
  ASSERT_EQ(plan.cantidadDeProcesosActivos(), 1);    
  ASSERT_EQ(plan.procesoEjecutado(), 1);  

  plan.reanudarProceso(0);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 2);
  ASSERT_EQ(plan.cantidadDeProcesosActivos(), 2);      
  ASSERT_EQ(plan.procesoEjecutado(), 1);

  for( int i = 2; i < 500 ; i++) {
      plan.agregarProceso(i);
      plan.pausarProceso(i);
  }
  ASSERT_EQ(plan.cantidadDeProcesos(), 500);
  ASSERT_EQ(plan.cantidadDeProcesosActivos(), 2);      

}

void copiaBien() {
  PlanificadorRR<int> plan;
  plan.agregarProceso(1);
  plan.agregarProceso(2);
  plan.agregarProceso(3);
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 3);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);  
  
  PlanificadorRR<int> copia_plan = plan;
  
  copia_plan.agregarProceso(4);
  
  ASSERT_EQ(plan.hayProcesosActivos(), true);
  ASSERT_EQ(plan.cantidadDeProcesos(), 3);  
  ASSERT_EQ(plan.procesoEjecutado(), 1);  
  
  ASSERT_EQ(copia_plan.cantidadDeProcesos(), 4);  
  ASSERT_EQ(copia_plan.procesoEjecutado(), 1);  

}

void imprimeBien() {
    PlanificadorRR<int> plan;
    plan.agregarProceso(1);
    plan.agregarProceso(2);
    plan.agregarProceso(3);
    ASSERT_EQ(plan.hayProcesosActivos(), true);
    ASSERT_EQ(plan.cantidadDeProcesos(), 3); 
    ASSERT_EQ(plan.procesoEjecutado(), 1);  
    ASSERT_EQ(to_s(plan), "[1*, 2, 3]");
    plan.ejecutarSiguienteProceso();
    ASSERT_EQ(plan.cantidadDeProcesos(), 3); 
    ASSERT_EQ(plan.procesoEjecutado(), 2);  
    ASSERT_EQ(to_s(plan), "[2*, 3, 1]");
    
    plan.eliminarProceso(3);
    ASSERT_EQ(plan.cantidadDeProcesos(), 2); 
    ASSERT_EQ(plan.procesoEjecutado(), 2);  
    ASSERT_EQ(to_s(plan), "[2*, 1]");
    
    plan.agregarProceso(4);
    plan.pausarProceso(4);
    ASSERT_EQ(to_s(plan), "[2*, 1, 4 (i)]");
    
    plan.ejecutarSiguienteProceso();
    ASSERT_EQ(to_s(plan), "[1*, 4 (i), 2]");
    plan.ejecutarSiguienteProceso();
    ASSERT_EQ(to_s(plan), "[2*, 1, 4 (i)]");
    plan.pausarProceso(2);
    ASSERT_EQ(to_s(plan), "[1*, 4 (i), 2 (i)]");
    plan.pausarProceso(1);
    ASSERT_EQ(to_s(plan), "[1 (i), 4 (i), 2 (i)]");
    plan.reanudarProceso(2);
    ASSERT_EQ(to_s(plan), "[2*, 1 (i), 4 (i)]");
    
}

void validaActivoBien() {
    PlanificadorRR<string> plan;
    plan.agregarProceso("Chrome");
    plan.agregarProceso("Sublime Text 2");
    plan.agregarProceso("terminal");
    plan.estaActivo("terminal");
    ASSERT_EQ(plan.estaActivo("terminal"), true);
    plan.pausarProceso("terminal" );
    ASSERT_EQ(plan.estaActivo("terminal"), false);
    plan.reanudarProceso("terminal"); 
    ASSERT_EQ(plan.estaActivo("terminal"), true);
}

void validaPlanificadoBien() {
    PlanificadorRR<string> plan;
    plan.agregarProceso("Chrome");
    plan.agregarProceso("Sublime Text 2");
    plan.agregarProceso("terminal");
}

void validaIgualdadBien() {
    PlanificadorRR<string> plan;
    plan.agregarProceso("Chrome");
    plan.agregarProceso("Sublime Text 2");
    plan.agregarProceso("terminal");
    
    plan.ejecutarSiguienteProceso();
    
    PlanificadorRR<string> plan2(plan);
    
    ASSERT_EQ(plan==plan2, true);
    plan.detener();
    ASSERT_EQ(plan==plan2, false);
    plan2.detener();
    ASSERT_EQ(plan==plan2, true);
}

void validaPauseadoBien() {
    PlanificadorRR<string> plan;
    plan.agregarProceso("Chrome");
    plan.agregarProceso("Sublime Text 2");
    plan.agregarProceso("terminal");
    ASSERT_EQ(plan.detenido(), false);
    plan.detener();
    ASSERT_EQ(to_s(plan), "[Chrome*, Sublime Text 2, terminal]");
    ASSERT_EQ(plan.detenido(), true);
    ASSERT_EQ(plan.cantidadDeProcesos(), 3);
    ASSERT_EQ(plan.cantidadDeProcesosActivos(), 0);
    ASSERT_EQ(plan.hayProcesos(), true);
    ASSERT_EQ(plan.hayProcesosActivos(), false);
    
    plan.reanudar();
    
    ASSERT_EQ(plan.detenido(), false);
    ASSERT_EQ(plan.cantidadDeProcesos(), 3);
    ASSERT_EQ(plan.cantidadDeProcesosActivos(), 3);
    ASSERT_EQ(plan.hayProcesos(), true);
    ASSERT_EQ(plan.hayProcesosActivos(), true);
    plan.ejecutarSiguienteProceso();
    ASSERT_EQ(to_s(plan), "[Sublime Text 2*, terminal, Chrome]");
    
    
}
int main()
{
  RUN_TEST( planificadorVacio );
  RUN_TEST( planificadorConProcesos );
  RUN_TEST( planificadorAgregaProcesoEjecutaYAgrega );
  RUN_TEST( crearUnoBorrarlo );
  RUN_TEST( crearVariosYBorrarlo );
  RUN_TEST( crearEjecutarYBorrar );
  RUN_TEST( crearPausearReanudar );
  RUN_TEST( copiaBien );
  RUN_TEST( imprimeBien );
  RUN_TEST( validaActivoBien );
  RUN_TEST( validaPlanificadoBien );
  RUN_TEST( validaIgualdadBien );
  RUN_TEST( validaPauseadoBien );
  // AGREGAR MAS TESTS...

  return 0;
}

#ifndef PLANIFICADOR_RR_H_
#define PLANIFICADOR_RR_H_

#include <iostream>
#include <cassert>
using namespace std;

/**
 * Se puede asumir que el tipo T tiene constructor por copia y operator==
 * No se puede asumir que el tipo T tenga operator=
 */
template<typename T>
class PlanificadorRR {

  public:

	/**
	 * Crea un nuevo planificador de tipo Round Robin.
	 */	
	PlanificadorRR();

	/**
	 * Una vez copiado, ambos planificadores deben ser independientes, 
	 * es decir, por ejemplo, que cuando se borra un proceso en uno
	 * no debe borrarse en el otro.
	 */	
	PlanificadorRR(const PlanificadorRR<T>&);

	/**
	 * Acordarse de liberar toda la memoria!
	 */	 
	~PlanificadorRR();

	/**
	 * Agrega un proceso al planificador. El mismo debe ubicarse,
	 * dentro del orden de ejecución, inmediatamente antes del que está
	 * siendo ejecutado actualmente. Si no hubiese ningún proceso en ejecución,
	 * la posición es arbitraria y el proceso pasa a ser ejecutado automáticamente.
	 * PRE: El proceso no está siendo planificado por el planificador.
	 */
	void agregarProceso(const T&);

	/**
	 * Elimina un proceso del planificador. Si el proceso eliminado
	 * está actualmente en ejecución, automáticamente pasa a ejecutarse
	 * el siguiente (si es que existe).
	 * PRE: El proceso está siendo planificado por el planificador.
	 */
	void eliminarProceso(const T&);

	/**
	 * Devuelve el proceso que está actualmente en ejecución.
	 * PRE: Hay al menos un proceso activo en el planificador.
	 */
	const T& procesoEjecutado() const;

	/**
	 * Procede a ejecutar el siguiente proceso activo,
	 * respetando el orden de planificación.
	 * PRE: Hay al menos un proceso activo en el planificador.
	 */
	void ejecutarSiguienteProceso();

	/**
	 * Pausa un proceso por tiempo indefinido. Este proceso pasa
	 * a estar inactivo y no debe ser ejecutado por el planificador.
	 * Si el proceso pausado está actualmente en ejecución, automáticamente
	 * pasa a ejecutarse el siguiente (si es que existe).
	 * PRE: El proceso está siendo planificado por el planificador.
	 * PRE: El proceso está activo.
	 */
	void pausarProceso(const T&);

	/**
	 * Reanuda un proceso previamente pausado. Este proceso pasa a estar
	 * nuevamente activo dentro del planificador. Si no había ningún proceso
	 * en ejecución, el proceso pasa a ser ejecutado automáticamente.
	 * PRE: El proceso está siendo planificado por el planificador.
	 * PRE: El proceso está inactivo.
	 */
	void reanudarProceso(const T&);

	/**
	 * Detiene la ejecución de todos los procesos en el planificador
	 * para atender una interrupción del sistema.
	 * PRE: El planificador no está detenido.
	 */
	void detener();

	/**
	 * Reanuda la ejecución de los procesos (activos) en el planificador
	 * luego de atender una interrupción del sistema.
	 * PRE: El planificador está detenido.
	 */
	void reanudar();

	/**
	 * Informa si el planificador está detenido por el sistema operativo.
	 */
	bool detenido() const;

	/**
	 * Informa si un cierto proceso está siendo planificado por el planificador.
	 */
	bool esPlanificado(const T&) const;

	/**
	 * Informa si un cierto proceso está activo en el planificador.
	 * PRE: El proceso está siendo planificado por el planificador.
	 */
	bool estaActivo(const T&) const;

	/**
	 * Informa si existen procesos planificados.
	 */
	bool hayProcesos() const;

	/**
	 * Informa si existen procesos activos.
	 */
	bool hayProcesosActivos() const;

	/**
	 * Devuelve la cantidad de procesos planificados.
	 */
	int cantidadDeProcesos() const;

	/**
	 * Devuelve la cantidad de procesos planificados y activos.
	 */
	int cantidadDeProcesosActivos() const;

	/**
	 * Devuelve true si ambos planificadores son iguales.
	 */
	bool operator==(const PlanificadorRR<T>&) const;

	/**
	 * Debe mostrar los procesos planificados por el ostream (y retornar el mismo).
	 * Los procesos deben aparecer en el mismo orden en el que son ejecutados
	 * por el planificador. Como la lista es circular, se decidió que el primer
	 * proceso que se muestra debe ser el que está siendo ejecutado en ese momento.
	 * En el caso particular donde exista al menos un proceso planificado,
	 * pero estén todos pausados, se puede comenzar por cualquier proceso.
	 * Un proceso inactivo debe ser identificado con el sufijo ' (i)'
	 * y el proceso que está siendo ejecutado, de existir, debe ser identificado
	 * con el sufijo '*'.
	 * PlanificadorRR vacio: []
	 * PlanificadorRR con 1 elemento activo: [p0*]
	 * PlanificadorRR con 2 elementos inactivos: [p0 (i), p1 (i)]
	 * PlanificadorRR con 3 elementos (p0 inactivo, p2 siendo ejecutado: [p2*, p0 (i), p1]
	 *
	 * OJO: con pX (p0, p1, p2) nos referimos a lo que devuelve el operador <<
	 * para cada proceso, es decir, cómo cada proceso decide mostrarse en el sistema.
	 * El sufijo 'X' indica el orden relativo de cada proceso en el planificador.
	 */
	ostream& mostrarPlanificadorRR(ostream&) const;

  private:
  
	/**
	 * No se puede modificar esta funcion.
	 */
	PlanificadorRR<T>& operator=(const PlanificadorRR<T>& otra) {
		assert(false);
		return *this;
	}

	/**
	 * Aca va la implementación del nodo.
	 */
	struct Nodo {
		T proceso;
        bool activo;
        Nodo* sig;
        Nodo(const T& t, bool a)  : proceso(t) {
            activo = a;
        }
	};
    // Lo guardo para saber donde poder agregar nuevos procesos
    Nodo* anterior;
    
    // Lo guardo para pedir el siguiente proceso
    Nodo* actual;

    // Lo guardo para saber si esta pauseado o no el planificador
    bool pauseado;
};

template<class T>
ostream& operator<<(ostream& out, const PlanificadorRR<T>& a) {
	return a.mostrarPlanificadorRR(out);
}

template<class T>
PlanificadorRR<T>::PlanificadorRR() {
    actual = NULL;
    anterior = NULL;
    pauseado = false;
}

template<class T>
PlanificadorRR<T>::PlanificadorRR(const PlanificadorRR<T>& copia) {
    pauseado = copia.pauseado;
    
    //hay uno solo
    if (copia.actual == copia.anterior && copia.actual != NULL) {
        Nodo* n = new Nodo(copia.actual->proceso, copia.actual->activo);
        n->sig = n;
        actual = n;
        anterior = n;
    } else {
        //hay mas de uno
        if (copia.actual != NULL) {
            Nodo* act = copia.actual;
            Nodo* n = new Nodo(act->proceso, act->activo);
            n->sig = n;
            actual = n;
            anterior = n;
            act=act->sig;
            while(act != NULL && act != copia.actual) {
                Nodo* n = new Nodo(act->proceso, act->activo);
                n->sig = actual;
                anterior->sig = n;
                anterior = n;
                act=act->sig;
            }        
        } else {
            //no hay nada
            actual = NULL;
            anterior = NULL;
        }
        
    }
    
}

template<class T>
void PlanificadorRR<T>::agregarProceso(const T& proceso) {
    Nodo* nuevo = new Nodo(proceso, true);
    nuevo->activo = true;
  
    //sino hay nada
    if (actual==NULL && anterior==NULL) {
        actual = nuevo;
        anterior = nuevo;
        anterior->sig = nuevo;
        nuevo->sig = actual;
    } else {
        
        if (actual == anterior) {
            //si hay uno
            nuevo->sig = actual;
            actual->sig = nuevo;
            if(!hayProcesosActivos()) {
                actual = nuevo;
            } else {
                anterior = nuevo;
            }
    
        } else {
            //si hay mas de uno
            nuevo->sig = actual;
            anterior->sig = nuevo;
            if(!hayProcesosActivos()) {
                anterior = nuevo;
                actual = nuevo;
            } else {
                anterior = nuevo;
            }

        }
        
    }
}

template<class T>
bool PlanificadorRR<T>::hayProcesos() const {
    return this->actual != NULL;
}

template<class T>
bool PlanificadorRR<T>::hayProcesosActivos() const {
    if(pauseado) {
        return false;
    }
    Nodo* act = this->actual;
    while(act != NULL && !act->activo && act != this->anterior) {
        act = act->sig;
    }

   	if (act == NULL) {
   		return false;
   	} else {
   		return (anterior == actual && actual->activo) || act != anterior;
   	}

}

template<class T>
ostream& PlanificadorRR<T>::mostrarPlanificadorRR(ostream& os) const {
    os << "[";
    Nodo* act = actual;
    if (act != NULL ) {
        if (act->activo) {
            if(actual != anterior) {
                os << act->proceso << "*, ";    
            } else {
                os << act->proceso << "*";
            }
        } else {
            if(actual != anterior) {
                os << act->proceso << " (i), ";    
            } else {
                os << act->proceso << " (i)";
            }
        }
        act = act->sig;
    }
    while( act != NULL && act != anterior) {
        if (!act->activo) {
            os << act->proceso << " (i), "; 
        } else {
            os << act->proceso << ", ";
        }
        act = act->sig;
    }
    if (act != NULL && actual != anterior && act == anterior) {
        if (!act->activo) {
            os << act->proceso << " (i)"; 
        } else {
            os << act->proceso;
        }
    }
    os << "]";
}

template<class T>
PlanificadorRR<T>::~PlanificadorRR() {
    if (actual != NULL) {
        anterior->sig = NULL;
        while(actual->sig != NULL) {
            Nodo* aux = actual;
            actual = actual->sig;
            delete aux;    
        }
        delete actual;
        actual = NULL;
        anterior = NULL;
        pauseado = false;
    }
}

template<class T>
int PlanificadorRR<T>::cantidadDeProcesos() const {
    int cant = 0;
    Nodo* act = actual;
    while( act != NULL && act != anterior) {
        cant++;
        act = act->sig;
    }
    if (act != NULL && act == anterior) {
        cant++;
    }
    return cant;
}

template<class T>
int PlanificadorRR<T>::cantidadDeProcesosActivos() const {
    int cant = 0;
    if(pauseado) {
        return cant;
    }
    Nodo* act = actual;
    while( act != NULL && act != anterior) {
        if(act->activo) cant++;
        act = act->sig;
    }
    if (act != NULL && act == anterior) {
         if(act->activo) cant++;
    }
    return cant;
}

template<class T>
const T& PlanificadorRR<T>::procesoEjecutado() const {
	return this->actual->proceso;
}

template<class T>
void PlanificadorRR<T>::ejecutarSiguienteProceso() {
    assert(!detenido());
	Nodo* act = actual;
	while(!act->sig->activo) {
		act = act->sig;
	}
	actual = act->sig;
	anterior = act;
}

template<class T>
//pre: el proceso existe
void PlanificadorRR<T>::eliminarProceso(const T& proceso) {
	Nodo* act = actual;
	Nodo* ant = anterior;
    
    if(anterior == actual) {
		delete actual;
        anterior = NULL;
		actual = NULL;
    } else {
        while(!(act->proceso == proceso)) {
		    ant = act;
		    act = act->sig;
	    }
        
        if (act == actual) {
            actual = act->sig;
            ant->sig = act->sig;
        } else {
           if(actual->sig == anterior) {
               anterior = ant;
           }
        }
        ant->sig = act->sig;
        
        delete act;    
    }
}


template<class T>
void PlanificadorRR<T>::pausarProceso(const T& proceso) {
	Nodo* act = actual;
	while(!(act->proceso == proceso)) {		
		act = act->sig;
	}
	if (act == actual) {
		ejecutarSiguienteProceso();
	}
	act->activo = false;
}


template<class T>
void PlanificadorRR<T>::reanudarProceso(const T& proceso) {
	Nodo* act = actual;
	while(!(act->proceso == proceso)) {		
		act = act->sig;
	}
	if (!hayProcesosActivos()) {
        act->activo = true;
        ejecutarSiguienteProceso();
            
	}  else {
        act->activo = true;
    }
	
}


template<class T>
void PlanificadorRR<T>::detener() {
    pauseado = true;
}

template<class T>
void PlanificadorRR<T>::reanudar() {
    pauseado = false;
}

template<class T>
bool PlanificadorRR<T>::detenido() const {
    return pauseado;
}

template<class T>
bool PlanificadorRR<T>::esPlanificado(const T& proceso) const {
    Nodo* act = actual;
	Nodo* ant = anterior;
    if(act == NULL) {
        return false;
    }
	while(act != ant && !(act->proceso == proceso)) {		
		act = act->sig;
	}
	return act->proceso == proceso;
}

template<class T>
bool PlanificadorRR<T>::estaActivo(const T& proceso) const {
    Nodo* act = actual;
	while(!(act->proceso == proceso)) {		
		act = act->sig;
	}
	return act->activo;
}

template<class T>
bool PlanificadorRR<T>::operator==(const PlanificadorRR<T>& p) const {
    ostringstream os;
    os << (*this);
    ostringstream os2;
    os2 << p;
    return detenido() == p.detenido() && os.str() == os2.str();
}


#endif // PLANIFICADOR_RR_H_
